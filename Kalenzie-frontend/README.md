# [Kalenzie]
//TODO This is a detailed description of your project.
## Project Requirements
Once your project has been completed please fill out the links below. These are a part of the rubric and must be completed before the submission deadline
**[Scrum Board URL](https://trello.com/invite/b/C4IJDb5R/429df15df3b9be12b986daa3b55c918a/kalenzie-trello)** |
**[Deployment URL](https://kalenzie.vercel.app/)** |
**[Repo URL](https://gitlab.com/drewskiiiiiiiii/kalenzie/)** |
**[Kapstone Pitch URL](https://docs.google.com/document/d/1ja152s5KNLIh_mwRtfRy6aWgKpy6g_-EaZQFzMTZDUg/edit)** |
**[Group Retrospective Document URL](https://docs.google.com/document/d/1zZ1IhkcHrrOsYh5B7lfbFwErDR5aS_ox4Q1R6JhHE9k/edit)** |
**[Kapstone Feedback Form](https://docs.google.com/forms/d/1yeIyQH6ZE6y5Z0qB2i8yW5_1Gzfxs8YiJsNlcyjR0WA/edit)**
> **Note:**  **Points will be deducted** if this is not filled in correctly. Also each one of these **MUST** live in their own document. For example DO **NOT** put your retrospective in your scrum board as it will not be counted and you will lose points.
## About Us
Tell us about your team, who was involved, what role did they fulfill and what did they work on (the more details the better). Also please choose ONE of the staff below that was your investor and remove the others.
|      Name          |Scrum Role                          |I worked on                         |
|----------------|-------------------------------|-----------------------------|
|Kano|`Product Investor`            |Investing            |
|Edward Perkins |`QA`            |Linked up front and back ends, polished auth features, enabled private routes so certain pages are only accessible depending on who is logged in, hooked up calendar component library |
|Drew Sexton |`Product Owner`| initialized mongoose db, created user schema, took charge of availability details (finding, updating), ensured merges were complete and up-to-date |
|Shavonne Carson |`Scrum Master`| helped implement firebase auth |
|Jonny Sueck |`Dev`| helped design and structure the front end react pages, and in the back end i worked on Auth with firebase and displaying user information from the mongo database |

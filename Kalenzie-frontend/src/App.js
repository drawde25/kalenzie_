import React, {useEffect} from 'react';
import './App.css';
import { observer } from 'mobx-react';
import { Navigation } from './Navigation';
import {AuthProvider} from './auth/AuthProvider'
import {app} from './pages/register/register'
import 'semantic-ui-css/semantic.min.css'
import store from "./store";

const App = observer(() => {
  
  const saveCoaches = async() =>
{
  try {
      await store.getCoaches()
  } catch (err) {
      console.log(err)
  }
}
  useEffect(()=>{
    saveCoaches();
  })

  function openNav() {
    document.getElementById("root").style.marginLeft = "30%"
    document.getElementById("screen").style.marginLeft = "30%"
    document.getElementById("sideBar").style.width = "30%";
    document.getElementById("sideBar").style.display = "block";
  }
  
  function closeNav() {
    document.getElementById("sideBar").style.width = "0";
    document.getElementById("root").style.marginLeft = "0%"
    document.getElementById("screen").style.marginLeft = "0%"

  }
  
  const signOut = (e) =>
{
    e.preventDefault()
        app
        .auth()
        .signOut()
        .then(function() {
            // Sign-out successful.
            console.log('@@')
        })
        .catch(function(error) {
            // An error happened.
            console.log(error);
        });
} 
  
  if(localStorage.getItem('role') === 'student')
  {
    return ( 
    <AuthProvider>
      <div className = "App" id="screen">
      <div id="main">
         <img src="https://i.imgur.com/3TbWA5n.png" alt='logo' id="kalenzieLogo"/>
        <a id="menu" onClick={openNav}>&#9776; Menu</a>
          </div>
          <div id='sideBar' className="sidenav">
            <br/><br/>
            <a id="close" className="closebtn" onClick={closeNav}>X</a>
            <div id='navigation'>
            <button onClick= {signOut}> Sign Out</button>
              <a href='/'>Home</a>
              <a href='/edit-profile'>Edit Profile</a>
              <a href="/notifactions">Notifications</a>
              <a href="/Meetings">Meetings</a>
              
              <a href="/ScheduleDayView">Schedule meeting</a>
              
              </div>  
            </div>
          <Navigation/>
      </div>
    </AuthProvider>
  )
  }
  else{
    return ( 
      <AuthProvider>
        <div className = "App">
        <div className = "App" id="screen">
      <div id="main">
      <img src="https://i.imgur.com/3TbWA5n.png" alt='logo' id="kalenzieLogo"/>
        <a id="menu" onClick={openNav}>&#9776; Menu</a>
          </div>
          <div id='sideBar' className="sidenav">
            <br/><br/>
            <a id="close" className="closebtn" onClick={closeNav}>X</a>
            <h1> Kalenzie</h1>
            <br/><br/>
            <div id='navigation'>
            <button onClick= {signOut}> Sign Out</button>
              <a href='/'>Home</a>
              <a href='/availability'>Availability</a>
              <a href='/edit-profile'>Edit Profile</a>
              <a href="/notifactions">Notifications</a>
              <a href="/Meetings">Meetings</a>
              
              </div>
            </div>
          <Navigation/>
      </div>
      </div>
    </AuthProvider>
    )}
    
})

export default App;

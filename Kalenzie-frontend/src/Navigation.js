import React from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import {PrivateRoute} from "./auth/PrivateRoute";
import { AvailabilityPage } from './pages/availability/Availability'
import {ScheduleMeetingPage} from './pages/scheduleMeeting/ScheduleMeeting'
import {EditProfileLayout} from './pages/editProfile/EditProfileLayout'
import {Notifactions} from './pages/NotifactionsPage/Notifacations'
import {Meetings} from './pages/MeetingsPage/Meetings'
import {ScheduleMeetingTime} from './pages/scheduleMeetingTime/MeetingTime'
import {Login} from './pages/login/login'
import {Home} from './pages/home/Home'
import {TestPage} from './pages/tests/test'
import {Register} from './pages/register/register'
import {ScheduleDayView} from './pages/scheduleDayView/ScheduleDayView'

export const Navigation = () =>
{
    return(
        <BrowserRouter>
          <Switch>
            <PrivateRoute exact path='/'>
              <Home/>
            </PrivateRoute>
            <PrivateRoute  path='/availability'>
              <AvailabilityPage/>
            </PrivateRoute>
            <PrivateRoute path='/scheduleMeeting'>
              <ScheduleMeetingPage/>
            </PrivateRoute>
            <PrivateRoute path="/edit-profile">
              <EditProfileLayout/>
            </PrivateRoute>
            <PrivateRoute path="/notifactions">
              <Notifactions/>
            </PrivateRoute>
            <PrivateRoute path="/Meetings">
              <Meetings/>
            </PrivateRoute>
            <Route path="/Login">
              <Login/>
            </Route>
            <Route path='/Register'>
              <Register />
            </Route>
            <PrivateRoute path="/ScheduleDayView">
              <ScheduleDayView/>
            </PrivateRoute>
            <PrivateRoute path='/ScheduleMeetingTime'>
              <ScheduleMeetingTime/>
            </PrivateRoute>
            <Route path='/test'>
              <TestPage/>
            </Route>
          </Switch>
        </BrowserRouter>
    )
}  


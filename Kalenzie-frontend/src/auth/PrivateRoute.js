import React, {useContext } from "react";
import { Route, Redirect } from "react-router-dom";
import { AuthContext } from "./AuthProvider";

export const PrivateRoute = ({children, ...rest }) => {
  // console.log(useContext(AuthContext)) 
  const {currentUser} = useContext(AuthContext)
  if(currentUser)
  {
      return(
      <Route {...rest}>
          {children}
      </Route>)
  }
  else
  {
      return(
          <>
            <Redirect to={"/Login"} />
          </>
      )
  }
};

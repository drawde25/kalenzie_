import React ,{useState} from 'react'
import './meeting.css'

export const Meeting = (props) =>
{
    // useEffect(async()=>{
    //     const url ="http://localhost:5000/getMeeting/no"
    //     const response = await fetch(url)
    //     const data = await response.json()
    //     console.log(data)
    // },[])
    const [isShown, setIsShown] = useState({isShown:false});
    const handleToggle = () =>
   {
      
       if(!isShown.isShown)
       {
        setIsShown((prevState)=> ({...prevState,isShown:true}))

       }
       else
       {
        setIsShown((prevState)=> ({...prevState,isShown:false}))
       }
        
   }
   const handleDelete = async() =>
   {    
       
       try {
        const url = `http://localhost:5000/deleteMeeting/${props.id}`
        const response = await fetch(url,{method: 'DELETE'})
        const data = await response.json()
        console.log(data) 
        
       } catch (err) {
         console.log(err)  
       }
   }
  
   
    return(
        <>
            <div>
                <p>Date: {props.date}</p>
                <p>Time: {props.time}</p>
                <p>Sender: {props.sender}</p>
                
                <div className ={!isShown.isShown?'hidden':'shown'} >
                    <p>Message: {props.message}</p>
                    <p>Status: {props.status}</p>
                    <button onClick ={handleDelete}>Cancel Meeting?</button>
                </div>
                
                <button onClick = {handleToggle} >{!isShown.isShown?'Show More?':'Show Less?'}</button>
                <br/>
                <br/>
            </div>
        </>
    )
}
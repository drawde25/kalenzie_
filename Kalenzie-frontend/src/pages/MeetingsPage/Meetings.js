import React from 'react'
import {Meeting} from './Meeting'
import { useEffect,useState } from 'react'




export const Meetings = () => {
    useEffect(()=>{
        getMeetings()
    },[])
    const [meetings,setMeetings] = useState({meetings: []})
    const username = localStorage.getItem('username')
    const getMeetings = async()=>{
    const url =`http://localhost:5000/getMeeting/${username}`
        try {
            const response = await fetch(url)
            const data = await response.json()
            setMeetings((prevstate) => ({meetings: data}))  
        } catch (err) {
            console.log(err)
        }    
   }
   
    
    return(
        <>
        <h1>Meetings</h1>
          {meetings.meetings.map((meeting)=>
          
            (<Meeting
            id= {meeting._id}
            date ={meeting.date}
            time ={meeting.time}
            sender ={meeting.meetingWith}
            message = {meeting.message}
            status = {meeting.status}
            key={meeting._id}
            />)
          )}
        </>
    )
}
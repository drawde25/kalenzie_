import React ,{useState,useEffect} from 'react'
import {Notifaction} from './Notifaction'






export const Notifactions = () =>
{

    const username = localStorage.getItem('username')
    useEffect(()=>{
        getNotifactions()
    },[])
    const [notifactions, setNotifactions] = useState({notifactions: []})
    const getNotifactions = async() =>
    {
        try {
            const url = `http://localhost:5000/getNotifaction/${username}`
            const response = await fetch(url)
            const data = await response.json()
            setNotifactions(()=> ({notifactions: data}))
            
        } catch (err) {
            console.log(err)
        }
    }
    // const notifactions =
    // [
    //     {
    //         date: '10-5-2020',
    //         time: '6:30pm',
    //         message: 'need help with jsx',
    //         sender: 'person'
    //     },{
    //         date: '10-6-2020',
    //         time: '7:30pm',
    //         message: 'need help with redux',
    //         sender: 'person2'
    //     }
    // ]
    return(
        <>
        <h3>Notifications</h3>
        <hr />
          {notifactions.notifactions.map((notifaction)=>
          
            (<Notifaction
            date ={notifaction.date}
            time ={notifaction.time}
            meetingWith ={notifaction.meetingWith}
            message = {notifaction.message}
            id= {notifaction._id}
            meetingId = {notifaction.meetingId}
            />)
          )}
        </>
    )
}
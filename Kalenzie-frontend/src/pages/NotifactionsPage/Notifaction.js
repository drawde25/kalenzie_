import React from "react";
import axios from "axios";
export const Notifaction = (props) => {
  const username = localStorage.getItem("username");

  const postMeeting = async (data) => {
    const url = "http://localhost:5000/addMeeting";
    try {
        const meetingData = {
          time: data.time,
          date: data.date,
          username: username,
          message: data.message,
          meetingWith: data.username,
          status: 'confirmed'
        };
      const response = await axios.post(url, meetingData);
    } catch (error) {
      console.log(error);
    }
  };

  const deleteRequest = async () => {
    try {
      const url = `http://localhost:5000/deleteNotifaction/${props.id}`;
      const response = await fetch(url, { method: "DELETE" });
      const data = await response.json();
      console.log(data);
    } catch (err) {
      console.log(err);
    }
  };
  const confirmMeeting = async (data) => {
    const url = `http://localhost:5000/updateMeeting/${data._id}`;
    try {
      await axios.patch(url, { status: "confimed" });
    } catch (err) {
      console.log(err);
    }
  };
  const sendNotifaction = async (data) => {
    const url = `http://localhost:5000/addNotifaction`;
    const notifactionData = {
      time: data.time,
      date: data.date,
      username: data.username,
      message: `meeting with ${username} on ${data.date} at ${data.time} has been confirmed`,
      meetingWith: username,
      meetingId: data._id,
    };
    try {
      const response = await axios.post(url, notifactionData);
      console.log(response);
    } catch (err) {
      console.log(err);
    }
  };
  const getMeeting = async () => {
    const url = `http://localhost:5000/getMeetingI/${props.meetingId}`;
    try {
      const response = await axios.get(url);
      let data = response.data[0];
      confirmMeeting(data);
      sendNotifaction(data);
      postMeeting(data)
      alert('Meeting accepted')
      window.location.href='/Meetings'
    } catch (err) {
      console.log(err);
    }
  };
  const handleDelete = async () => {
    if (localStorage.getItem("role") === "coach") {
      getMeeting();
      deleteRequest();
    } else {
      deleteRequest();
    }
  };
  return (
    <>
      <p>date: {props.date}</p>
      <p>time: {props.time}</p>
      <p>message: {props.message}</p>
      <p>sender: {props.meetingWith}</p>
      <br />
      <br />
      <button onClick={handleDelete}> accept</button>
    </>
  );
};

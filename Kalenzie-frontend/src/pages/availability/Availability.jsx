import React, { useState } from "react";
import { observer } from "mobx-react";
import "./Availability.css";
import store from "../../store";
import {Button, Form, Input, Label} from 'semantic-ui-react'

export const AvailabilityPage = observer(() => {
  const availabilityNull = {
    monday: {
      startTime: null,
      endTime: null,
    },
    tuesday: {
      startTime: null,
      endTime: null,
    },
    wednesday: {
      startTime: null,
      endTime: null,
    },
    thursday: {
      startTime: null,
      endTime: null,
    },
    friday: {
      startTime: null,
      endTime: null,
    },
    saturday: {
      startTime: null,
      endTime: null,
    },
    sunday: {
      startTime: null,
      endTime: null,
    },
  }
  const [startTime, setStartTime] = useState(null);
  const [username, setUsername] = useState(localStorage.username)
  const [currentUser, setCurrentUser] = useState(JSON.parse(localStorage.user));
  const [endTime, setEndTime] = useState(null);
  const [selectedDays, setSelectedDays] = useState({
    monday: false,
    tuesday: false,
    wednesday: false,
    thursday: false,
    friday: false,
    saturday: false,
    sunday: false,
  });
  const [availability, setAvailability] = useState({
    monday: {
      startTime: null,
      endTime: null,
    },
    tuesday: {
      startTime: null,
      endTime: null,
    },
    wednesday: {
      startTime: null,
      endTime: null,
    },
    thursday: {
      startTime: null,
      endTime: null,
    },
    friday: {
      startTime: null,
      endTime: null,
    },
    saturday: {
      startTime: null,
      endTime: null,
    },
    sunday: {
      startTime: null,
      endTime: null,
    },
  });
  const updateDay = (day) => {
    if (day === "monday") {
      console.log(`updating Monday`);
      setAvailability((availability) => {
        return {
          ...availability,
          monday: {
            startTime: startTime,
            endTime: endTime,
          },
        };
      });
    }
    if (day === "tuesday") {
      setAvailability((availability) => {
        return {
          ...availability,
          tuesday: {
            startTime: startTime,
            endTime: endTime,
          },
        };
      });
    }
    if (day === "wednesday") {
      setAvailability((availability) => {
        return {
          ...availability,
          wednesday: {
            startTime: startTime,
            endTime: endTime,
          },
        };
      });
    }
    if (day === "thursday") {
      setAvailability((availability) => {
        return {
          ...availability,
          thursday: {
            startTime: startTime,
            endTime: endTime,
          },
        };
      });
    }
    if (day === "friday") {
      setAvailability((availability) => {
        return {
          ...availability,
          friday: {
            startTime: startTime,
            endTime: endTime,
          },
        };
      });
    }
    if (day === "saturday") {
      setAvailability((availability) => {
        return {
          ...availability,
          saturday: {
            startTime: startTime,
            endTime: endTime,
          },
        };
      });
    }
    if (day === "sunday") {
      setAvailability((availability) => {
        return {
          ...availability,
          sunday: {
            startTime: startTime,
            endTime: endTime,
          },
        };
      });
    }
  };
  const updateAvailability = (e) => {
    e.preventDefault();
    if (selectedDays.monday) {
      updateDay("monday");
    }
    if (selectedDays.tuesday) {
      updateDay("tuesday");
    }
    if (selectedDays.wednesday) {
      updateDay("wednesday");
    }
    if (selectedDays.thursday) {
      updateDay("thursday");
    }
    if (selectedDays.friday) {
      updateDay("friday");
    }
    if (selectedDays.saturday) {
      updateDay("saturday");
    }
    if (selectedDays.sunday) {
      updateDay("sunday");
    }
    // document.getElementById('availability-form').reset()
  };

  return (
    <>
      <header>
        <h3>Choose your Availabilty</h3>
        <h3>
          Current User: {localStorage.user ? localStorage.name : "N/A"}
          </h3> <hr />
        <br />
      </header>
      <div>
        <Form
          id='availability-form'
          onSubmit={(e) => {
            updateAvailability(e);
          }}
        >
          <Label> Available Days
          <div id="day-picker">
          <br /> 
            <Input
              type="checkbox"
              name="monday"
              value="monday"
              onChange={(e) => {
                setSelectedDays({
                  ...selectedDays,
                  monday: !selectedDays.monday,
                });
                console.log(`Monday selected`);
              }}
            />
            <label for="monday">Monday</label>
            <Input
              type="checkbox"
              name="tuesday"
              value="tuesday"
              onChange={(e) => {
                setSelectedDays({
                  ...selectedDays,
                  tuesday: !selectedDays.tuesday,
                });
                console.log(`Tuesday selected`);
              }}
            />
            <label for="tuesday">Tuesday</label>
            <Input
              type="checkbox"
              name="wednesday"
              value="wednesday"
              onChange={(e) => {
                setSelectedDays({
                  ...selectedDays,
                  wednesday: !selectedDays.wednesday,
                });
                console.log(`Wednesday selected`);
              }}
            />
            <label for="wednesday">Wednesday</label>
            <br />
            <br />
            <Input
              type="checkbox"
              name="thursday"
              value="thursday"
              onChange={(e) => {
                setSelectedDays({
                  ...selectedDays,
                  thursday: !selectedDays.thursday,
                });
                console.log(`Thursday selected`);
              }}
            />
            <label for="thursday">Thursday</label>
            <Input
              type="checkbox"
              name="friday"
              value="friday"
              onChange={(e) => {
                setSelectedDays({
                  ...selectedDays,
                  friday: !selectedDays.friday,
                });
                console.log(`Friday selected`);
              }}
            />
            <label for="friday">Friday</label>
            <Input
              type="checkbox"
              name="saturday"
              value="saturday"
              onChange={(e) => {
                setSelectedDays({
                  ...selectedDays,
                  saturday: !selectedDays.saturday,
                });
                console.log(`Saturday selected`);
              }}
            />
            <label for="saturday">Saturday</label>
            <Input
              type="checkbox"
              name="sunday"
              value="sunday"
              onChange={(e) => {
                setSelectedDays({
                  ...selectedDays,
                  sunday: !selectedDays.sunday,
                });
                console.log(`Sunday selected`);
              }}
            />
            <label for="sunday">Sunday</label>
          </div>
          </Label>
          <br /> <br />
          <Label>Available Times
          <div id="time-range">
          <br />
          <br />
            <div id="start-time">
              <label for="start-time">From:</label>
              <br />
              <Input
                type="time"
                name="start-time"
                id="start-time"
                onChange={(e) => {
                  e.preventDefault();
                  setStartTime(e.target.value);
                }}
              ></Input>
              
            </div>
            <div id="end-time">
              <label for="end-time">To:</label>
              <br />
              <Input
                type="time"
                name="end-time"
                id="end-time"
                onChange={(e) => {
                  e.preventDefault();
                  setEndTime(e.target.value);
                }}
              ></Input>
            </div>
          </div>
          </Label>
          <br />
          <br />
          <Input id="updateAvailability" type="submit" value="Update Availability" />
        </Form>
        <br />
        <table id="availability-display">
          <tr>
            <th>Day</th>
            <th>Current Availability</th>
            <th>New Availability</th>
          </tr>
          <tr>
            <td>Monday</td>
            <td>
              {currentUser.availability.monday.startTime} - {currentUser.availability.monday.endTime}
            </td>
            <td>
              {availability.monday.startTime} - {availability.monday.endTime}
            </td>
          </tr>
          <tr>
            <td>Tuesday</td>
            <td>
              {currentUser.availability.tuesday.startTime} - {currentUser.availability.tuesday.endTime}
            </td>
            <td>
              {availability.tuesday.startTime} - {availability.tuesday.endTime}
            </td>
          </tr>
          <tr>
            <td>Wednesday</td>
            <td>
              {currentUser.availability.wednesday.startTime} - {currentUser.availability.wednesday.endTime}
            </td>
            <td>
              {availability.wednesday.startTime} - {availability.wednesday.endTime}
            </td>
          </tr>
          <tr>
            <td>Thursday</td>
            <td>
              {currentUser.availability.thursday.startTime} - {currentUser.availability.thursday.endTime}
            </td>
            <td>
              {availability.thursday.startTime} - {availability.thursday.endTime}
            </td>
          </tr>
          <tr>
            <td>Friday</td>
            <td>
              {currentUser.availability.friday.startTime} - {currentUser.availability.friday.endTime}
            </td>
            <td>
              {availability.friday.startTime} - {availability.friday.endTime}
            </td>
          </tr>
          <tr>
            <td>Saturday</td>
            <td>
              {currentUser.availability.saturday.startTime} - {currentUser.availability.saturday.endTime}
            </td>
            <td>
              {availability.saturday.startTime} - {availability.saturday.endTime}
            </td>
          </tr>
          <tr>
            <td>Sunday</td>
            <td>
              {currentUser.availability.sunday.startTime} - {currentUser.availability.sunday.endTime}
            </td>
            <td>
              {availability.sunday.startTime} - {availability.sunday.endTime}
            </td>
          </tr>
        </table>
        <br />
        <Button
          onClick={(e) => {
            e.preventDefault();
            store.updateUserAvailability(username, {
              availability: availability,
            });
            setCurrentUser({
              ...currentUser,
              availability
            })
            console.log(
              `Availability on submit: ${JSON.stringify(availability)}`
            );
            setAvailability(availabilityNull)
          }}
        >
          Send Updated Avail.
        </Button>
      </div>
    </>
  );
});

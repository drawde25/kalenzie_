import React, { Component, useState } from "react";
import { Select, Button, Input, Form, Label } from "semantic-ui-react";
import axios from 'axios'
import './EditProfile.css'

export const EditProfileLayout =() => {
    
  const [state, setState] = useState({
      name: '',
      password: '',
      email: '',
      bio: '',
      quarter: '1',
      photo: '/kalenzie-user.png',
       
    })


  let loginData
  const username = localStorage.getItem('username')

  const saveData = ()=>
  {
      localStorage.setItem("bio",loginData.bio)
      localStorage.setItem("username",loginData.username)
      localStorage.setItem("name",loginData.name)
      localStorage.setItem("role",loginData.role)
      localStorage.setItem("photo",loginData.photo)
      localStorage.setItem("quarter",loginData.quarter)
      
  }
  const grabData = async(e) =>
  {
      
      const url =`http://localhost:5000/findUsersU/${username}`
      try {
          const response = await axios.get(url)
          const data = response.data[0]
          loginData =data
          console.log(loginData)
          saveData()
      } catch (err) {
          console.log(err)
      }
  }
  
  const handleChange = (e) => {
      const {id,value} = e.target
      console.log(value)
      setState(state => ({
          ...state,
          [id] : value
          
      }))
  }
  
  const handleSubmitBio = async(e) =>
  {
      e.preventDefault()
      const url = `http://localhost:5000/updateUser/${username}`
      try {
          const response = await axios.patch(url,{bio:state.bio})
          grabData()

      } catch (err) {
          console.log(err)
      }
  }
  const handleSubmitName = async(e) =>
  {
      e.preventDefault()
      const url = `http://localhost:5000/updateUser/${username}`
      try {
          const response = await axios.patch(url,{name:state.name})
          grabData()
      } catch (err) {
          console.log(err)
      }
  }
  const handleSubmitQuarter = async(e) => {
      e.preventDefault()
      const url = `http://localhost:5000/updateUser/${username}`
      try {
          const response = await axios.patch(url,{quarter:state.quarter})
          grabData()
      } catch (err) {
          console.log(err)
      }
  }
  const handleSubmitPassword = async(e) =>
  {
      e.preventDefault()
      const url = `http://localhost:5000/updateUser/${username}`
      try {
          const response = await axios.patch(url,{password:state.password})
          grabData()
      } catch (err) {
          console.log(err)
      }
  }
  const handleSubmitPhoto = async(e) =>
  {
      e.preventDefault()
      const url = `http://localhost:5000/updateUser/${username}`
      try {
          const response = await axios.patch(url,{photo:state.photo})
          grabData()
      } catch (err) {
          console.log(err)
      }
  }

    return (
    <div id="profileForum">
    <div className="header">Edit Profile 
    </div>
    <hr />
    <div className="forum">
    <Form onSubmit ={handleSubmitQuarter}>
          <Label>Quarter
          <select onChange={handleChange}  id='quarter'>
                        <option value="1">Q1</option>
                        <option value="2">Q2</option>
                        <option value="3">Q3</option>
                        <option value="4">Q4</option>
                    </select>
                    </Label>
                    <Button id="saveProfile" type='submit'>Save</Button>
        </Form>
        <hr />
        <Form onSubmit ={handleSubmitName}>
        <Label id="label">Name:
            <Input type="text" onChange={handleChange}  placeholder="name" id='name'></Input> </Label>
            <Button id="saveProfile" type='submit'>Save</Button> 
            <br />
            <br />
        </Form>

        <Form onSubmit ={handleSubmitPassword}>
            <Label id="label">Update Password
                <Input type="text" placeholder="password" onChange={handleChange}  id='password'></Input> 
            </Label>
            <Button id="saveProfile" type='submit'>Save</Button> 
        </Form>
            <br />
            <br />

        <Form onSubmit ={handleSubmitBio}>
        <Label id="label">Update Bio:
            <Input type="text" placeholder="bio" onChange={handleChange}  id='bio'></Input> 
            </Label>
            <Button id="saveProfile" type='submit'>Save</Button>
            <hr />
            <br />
        </Form>
        <br/>

        <Form onSubmit={handleSubmitPhoto}>
        <Label>Photo URL:
            <Input type="text" placeholder='photo' onChange={handleChange}  name="image" id="photo" />
            </Label>
            <Button>upload</Button>
            <br/> <br/>
            <Button id="saveProfile">Save</Button>
        </Form>
</div>
</div>
)
}

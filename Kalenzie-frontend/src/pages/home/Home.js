import React from "react";
import { Header } from "semantic-ui-react";

export const Home = () => {
  const name = localStorage.getItem("name");
  const username = localStorage.getItem("username");
  const role = localStorage.getItem("role");
  const photo = localStorage.getItem("photo");
  const quarter = localStorage.getItem("quarter");
  const bio = localStorage.getItem("bio");

  return (
    <>
      <Header>
        <h1>Profile</h1> <hr />
      </Header>
      <img src={photo} width="100" alt="user"></img>
      <p> Name: {name}</p>
      <p> Username: {username}</p>
      <p> Quarter: {quarter}</p>
      <p> Role: {role}</p>
      <p> Bio: {bio ? bio : "no bio"}</p>
    </>
  );
};

import Axios from "axios";
import React, { useState } from "react";
import "./login.css";
import { Button, Divider, Form } from "semantic-ui-react";
// Firebase App (the core Firebase SDK) is always required and
// must be listed before other Firebase SDKs
import firebase from "firebase/app";
import auth from "firebase/auth";

class Redirect extends React.Component {
  state = {
    redirect: false,
  };
  setRedirect = () => {
    this.setState({
      redirect: true,
    });
  };
  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to="/notifications" />;
    }
  };
  render() {
    return (
      <div>
        {this.renderRedirect()}
        <button onClick={this.setRedirect}>Redirect</button>
      </div>
    );
  }
}

export const Login = (props) => {
  let loginData;
  const [state, setState] = useState({
    email: "",
    password: "",
  });
  const handleChange = (e) => {
    const { id, value } = e.target;
    setState((state) => ({
      ...state,
      [id]: value,
    }));
  };
  const saveData = () => {
    console.log(loginData);
    localStorage.setItem("bio", loginData.bio);
    localStorage.setItem("username", loginData.username);
    localStorage.setItem("name", loginData.name);
    localStorage.setItem("role", loginData.role);
    localStorage.setItem("photo", loginData.photo);
    localStorage.setItem("quarter", loginData.quarter);
    localStorage.setItem("loggedIn", true);
    localStorage.setItem("user", JSON.stringify(loginData));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    const url = `http://localhost:5000/findUsers/${state.email}`;
    try {
      const response = await Axios.get(url);
      const data = response.data[0];
      loginData = data;
      saveData();
      authenticate();
    
    } catch (err) {
      
    }
  };
  
  const authenticate = async () => {
    try {
      await firebase
        .auth()
        .signInWithEmailAndPassword(state.email, state.password);
        alert('login successful')
      window.location.href = "/";
    } catch (err) {
        alert(`${err.message.split(" ").splice(0,7).join(" ")} login info provided`)
    }
  }

  return (
    <>
      <Form id="login-form" onSubmit={handleSubmit}>
        <h1>{props.greeting}</h1>
        <label for="email">Email</label>
        <input id="email" onChange={handleChange} placeholder="joe@example.com" />
        <br />
        <label for="password">Password</label>
        <input type='password' id="password" onChange={handleChange} placeholder="password" />
        <br />
        <Button type="submit" class="submit-btn">
          Log in
        </Button>
        <Divider horizontal>Or</Divider>
        <label>Don't have an account?</label>
        <a href="/Register">Register here</a>
      </Form>
    </>
  );
};
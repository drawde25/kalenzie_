import React, { useState } from "react";
import axios from "axios";
import "./register.css";
import { Form, Input, Button, Divider, Label } from "semantic-ui-react";
// Firebase App (the core Firebase SDK) is always required and
// must be listed before other Firebase SDKs
let firebase = require("firebase/app");

// Add the Firebase products that you want to use
require("firebase/auth");

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyCrSoH5WQaOFhuitaTVTeHHIHQ9ne8bM6I",
  authDomain: "kalenzie-661f1.firebaseapp.com",
  databaseURL: "https://kalenzie-661f1.firebaseio.com",
  projectId: "kalenzie-661f1",
  storageBucket: "kalenzie-661f1.appspot.com",
  messagingSenderId: "1040466989752",
  appId: "1:1040466989752:web:f76c71dfa72e6211301a4d",
  measurementId: "G-39CN4KDJES",
};
export const app = firebase.initializeApp(firebaseConfig);
export const Register = (props) => {
  const [state, setState] = useState({
    name: "",
    username: "",
    password: "",
    email: "",
    role: "student",
    bio: "",
    quarter: "1",
    photo: "/kalenzie-user.png",
  });

  const handleChange = (e) => {
    const { id, value } = e.target;
    setState((state) => ({
      ...state,
      [id]: value,
    }));
  };
  const createUser = async () => {
    const url = "http://localhost:5000/newUser";
    try {
      await axios.post(url, state);
      createUser();
    } catch (err) {
      console.log(err);
    }
  };
  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      await firebase
        .auth()
        .createUserWithEmailAndPassword(state.email, state.password);
      await createUser();
      alert("register succesfull now login");
      window.location.href = "/Login";
    } catch (error) {
      alert(`${error.message}`);
    }
  };

  return (
    <>
      <Form id="register-form">
        <h3>Register with Kalenzie</h3>
        <Form id="register" onSubmit={handleSubmit}>
          <Label>
            Username
            <Input id="username" required onChange={handleChange} />
          </Label>
          <hr />
          <Label>
            Name
            <Input id="name" required onChange={handleChange} />
          </Label>
          <hr />
          <Label>
            Email
            <Input id="email" required onChange={handleChange} />
          </Label>
          <hr />
          <Label>
            Password
            <Input id="password" required onChange={handleChange} />
          </Label>
          <hr />
          <Label>
            Quarter
            <select onChange={handleChange} id="quarter">
              <option value="1">Q1</option>
              <option value="2">Q2</option>
              <option value="3">Q3</option>
              <option value="4">Q4</option>
            </select>
          </Label>
          <hr />
          <hr />
          <Label>
            Student or Coach?
            <select onChange={handleChange} id="role">
              <option value="student">student</option>
              <option value="coach"> coach</option>
            </select>
          </Label>
          <hr />
          <Button type="submit" className="submit-btn" id="regButton">
            Register
          </Button>
        </Form>
        <Divider horizontal>Or</Divider>
        <p>
          Already have an account?
          <a href="/Login">login Here</a>
        </p>
      </Form>
    </>
  );
};

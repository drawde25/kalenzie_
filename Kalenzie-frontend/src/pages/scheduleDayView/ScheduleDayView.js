import React, { useEffect, useState } from "react";
import {Button, Input, Form, Label } from 'semantic-ui-react'
import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import interaction from "@fullcalendar/interaction";

export const ScheduleDayView = () => {
  const [showName, setShowName] = useState({ isShown: false });
  const [showDate, setShowDate] = useState({ isShown: false });

  const [date, setDate] = useState(localStorage.currentDate);
  const [times, setTimes] = useState([]);
  const [coaches, setCoaches] = useState(JSON.parse(localStorage.coaches));
  const [coachNames, setCoachNames] = useState(null)
  const [day, setDay] = useState(localStorage.dayOfWeek);
  useEffect(()=>{
    console.log(`coaches: ${coaches}`)
    localStorage.setItem('dayOfWeek', dateStringToDay(date))
    console.log(`day of week?: ${localStorage.dayOfWeek}`)
    seeCoaches(day)
  },[])

  const handleToggleDate = () => {
    if (!showDate.isShown) {
      setShowDate((prevState) => ({ ...prevState, isShown: true }));
    } else {
      setShowDate((prevState) => ({ ...prevState, isShown: false }));
    }
  };

  const handleDateSubmit = (e) => {
    e.preventDefault();
    localStorage.setItem("currentDate", date);
    window.location = "http://localhost:3000/ScheduleMeetingTime";
  };

  const handleToggleName = () => {
    if (!showName.isShown) {
      setShowName((prevState) => ({ ...prevState, isShown: true }));
    } else {
      setShowName((prevState) => ({ ...prevState, isShown: false }));
    }
  };

  const handleClick = (info) => {
    localStorage.setItem("currentDate", info.dateStr);
    window.location.href = "/ScheduleMeetingTime";
    // console.log(info)
  };

  const dateStringToDay = (dateStr) => {
    let queryDay = new Date(dateStr)
    switch(queryDay.getUTCDay()){
      case 0:
        localStorage.setItem('dayOfWeek', 'sunday')
        setDay('sunday')
        console.log('sunday')
        return 'sunday'
      case 1:
        localStorage.setItem('dayOfWeek', 'monday')
        setDay('monday')
        console.log('monday')
        return 'monday'
      case 2:
        localStorage.setItem('dayOfWeek', 'tuesday')
        setDay('tuesday')
        console.log('tuesday')
        return 'tuesday'
      case 3:
        localStorage.setItem('dayOfWeek', 'wednesday')
        setDay('wednesday')
        console.log('wednesday')
        return 'wednesday'
      case 4:
        localStorage.setItem('dayOfWeek', 'thursday')
        setDay('thursday')
        console.log('thursday')
        return 'thursday'
      case 5:
        localStorage.setItem('dayOfWeek', 'friday')
        setDay('friday')
        console.log('friday')
        return 'friday'
      case 6:
        localStorage.setItem('dayOfWeek', 'saturday')
        setDay('saturday')
        console.log('saturday')
        return 'saturday'
      default:
        return null
    }
  };

  const seeCoaches = async(day) => {
    let coachList = []
    coaches.map(coach=> coachList.push(coach.name))
    setCoachNames(coachList)
    return coachList
  };

  return (
    <>
    <hr />
      <br />
      <br />
      <h1>What day do you want to meet?</h1>
      {/* <Form className={showName.isShown ? "show" : "hidden"}>
        <Label htmlFor="searchName"> what coach ? </Label>
        <Input type="text" name="seachName" autoFocus />
        <Button type="submit" onClick={(handleToggleName, handleToggleDate)}>
          {" "}
          find{" "}
        </Button>
      </Form>
      <Button
        onClick={handleToggleName}
        className={showName.isShown ? "hidden" : "show"}
      >
        search by name
      </Button> */}
      <Form
        className={showDate.isShown ? "show" : "hidden"}
        onSubmit={handleDateSubmit}
      >
        <Label htmlFor="searchDate"> what date ? </Label>
        <Input
          type="date"
          name="searchDate"
          autoFocus
          value={date}
          onChange={(e) => {
            e.preventDefault();
            console.log(e.target.value);
            setDate(e.target.value);
          }}
        />
        <Button type="submit" onClick={(handleToggleDate, handleToggleName)}>
          Schedule a Meeting
        </Button>
      </Form>
      {/* <Button
        onClick={handleToggleDate}
        className={showDate.isShown ? "hidden" : "show"}
      >
        search by date{" "}
      </Button> */}
      <hr />
      <FullCalendar
        plugins={[dayGridPlugin, interaction]}
        initialView="dayGridMonth"
        dateClick={handleClick}
      />
    </>
  );
};

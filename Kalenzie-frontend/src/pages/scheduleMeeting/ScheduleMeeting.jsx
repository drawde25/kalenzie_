import React, { useState } from "react";
import "./ScheduleMeeting.css";
import axios from "axios";
import { useEffect } from "react";
export const ScheduleMeetingPage = () => {
  const username = localStorage.getItem("username");
  let meetingData;
  let notifactionData;
  let id;
  const date = localStorage.currentDate;
  const time = localStorage.chosenTime;
  const instructor = localStorage.chosenCoach;
  const [message, setMessage] = useState("");

  useEffect(() => {
    console.log(JSON.parse(localStorage.coaches));
  }, []);
  const postMeeting = async () => {
    const url = "http://localhost:5000/addMeeting";
    try {
      const response = await axios.post(url, meetingData);
      id = response.data._id;
      notifactionData = {
        time: time,
        date: date,
        username: instructor,
        message: message,
        meetingWith: username,
        meetingId: id,
      };
      postNotification();
      alert("meeting successfully requested");
      window.location.href = "/Meetings";
      console.log(id);
    } catch (error) {
      console.log(error);
      alert("something went wrong");
    }
  };

  const postNotification = async () => {
    const url = "http://localhost:5000/addNotifaction";
    try {
      await axios.post(url, notifactionData);
    } catch (error) {
      console.log(error);
    }
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    meetingData = {
      time: time,
      date: date,
      username: username,
      message: message,
      meetingWith: instructor,
      status: "not confirmed",
    };
    postMeeting();
  };
  const handleClick = () => {
    window.location.href = "/ScheduleMeetingTime";
  };
  return (
    <>
      <h1>Schedule a Meeting:</h1>
      <form id="schedule-meeting-form" onSubmit={handleSubmit}>
        <p>{`scheduleing a meeting with ${instructor} on ${date} at ${time}`}</p>
        <button onClick={handleClick}>change meeting info?</button>
        <br />

        <label for="meeting-notes">What is this meeting about?</label>
        <br />
        <input
          type="text"
          name="meeting-notes"
          id="meeting-notes"
          required
          onChange={(e) => {
            e.preventDefault();
            setMessage(e.target.value);
          }}
        />
        <br />
        <br />
        <input type="submit" value="Request Meeting" />
      </form>
    </>
  );
};

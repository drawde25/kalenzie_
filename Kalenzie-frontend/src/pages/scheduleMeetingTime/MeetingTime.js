import React, { useState } from "react";
import store from "../../store";
import { useEffect } from "react";
import { v4 } from "uuid";
import { Label, Form, Input } from "semantic-ui-react";
import "./MeetingTime.css";

export const ScheduleMeetingTime = () => {
  const [date, setDate] = useState(localStorage.currentDate);
  const [instructor, setInstructor] = useState(
    JSON.parse(localStorage.coaches)[0].name
  );
  const [day, setDay] = useState(localStorage.dayOfWeek);
  const [coaches, setCoaches] = useState(JSON.parse(localStorage.coaches));
  const [times, setTimes] = useState([900]);
  const [coachNames, setCoachNames] = useState(null);
  const [meetingTime, setMeetingTime] = useState(localStorage.chosenTime);

  useEffect(() => {
    let allCoaches = [];
    let parsedCoaches = JSON.parse(localStorage.coaches);
    parsedCoaches.map((coach) => {
      allCoaches.push(coach.name);
      getDailyTimes(coach, day);
    });
    // setCoaches(parsedCoaches)
    setCoachNames(allCoaches);
    localStorage.setItem("chosenCoach", instructor);
    localStorage.setItem("chosenTime", meetingTime);
    setMeetingTime(localStorage.chosenTime);
  }, [localStorage.dayOfWeek]);

  const handleSubmit = async () => {
    let coachList = await store.getCoaches();
    localStorage.setItem("currentDate", date);
    localStorage.setItem("dayOfWeek", dateStringToDay(date));
    localStorage.setItem("coaches", coachList);
    setCoaches(coachList);
  };

  const dateStringToDay = (dateStr) => {
    let queryDay = new Date(dateStr);
    let dayOfWeek;
    switch (queryDay.getUTCDay()) {
      case 0:
        localStorage.setItem("dayOfWeek", "sunday");
        setDay("sunday");
        return "sunday";
      case 1:
        localStorage.setItem("dayOfWeek", "monday");
        setDay("monday");
        return "monday";
      case 2:
        localStorage.setItem("dayOfWeek", "tuesday");
        setDay("tuesday");
        return "tuesday";
      case 3:
        localStorage.setItem("dayOfWeek", "wednesday");
        setDay("wednesday");
        return "wednesday";
      case 4:
        localStorage.setItem("dayOfWeek", "thursday");
        setDay("thursday");
        return "thursday";
      case 5:
        localStorage.setItem("dayOfWeek", "friday");
        setDay("friday");
        return "friday";
      case 6:
        localStorage.setItem("dayOfWeek", "saturday");
        setDay("saturday");
        return "saturday";
      default:
        return null;
    }
  };

  const getDailyTimes = (coach, day) => {
    let timeArr = [];
    let startTime = null;
    let endTime = null;
    switch (day) {
      case "monday":
        if (coach.availability.monday.startTime) {
          startTime =
            Number(coach.availability.monday.startTime.slice(0, 2)) * 100;
          endTime = Number(coach.availability.monday.endTime.slice(0, 2)) * 100;

          for (let i = startTime; i < endTime; i += 100) {
            timeArr.push(i);
            timeArr.push(i + 30);
          }
        } else {
          startTime = null;
          endTime = null;
        }
        break;
      case "tuesday":
        if (coach.availability.tuesday.startTime) {
          startTime =
            Number(coach.availability.tuesday.startTime.slice(0, 2)) * 100;
          endTime =
            Number(coach.availability.tuesday.endTime.slice(0, 2)) * 100;

          for (let i = startTime; i < endTime; i += 100) {
            timeArr.push(i);
            timeArr.push(i + 30);
          }
        } else {
          startTime = null;
          endTime = null;
        }
        break;
      case "wednesday":
        if (coach.availability.wednesday.startTime) {
          startTime =
            Number(coach.availability.wednesday.startTime.slice(0, 2)) * 100;
          endTime =
            Number(coach.availability.wednesday.endTime.slice(0, 2)) * 100;

          for (let i = startTime; i < endTime; i += 100) {
            timeArr.push(i);
            timeArr.push(i + 30);
          }
        } else {
          startTime = null;
          endTime = null;
        }
        break;
      case "thursday":
        if (coach.availability.thursday.startTime) {
          startTime =
            Number(coach.availability.thursday.startTime.slice(0, 2)) * 100;
          endTime =
            Number(coach.availability.thursday.endTime.slice(0, 2)) * 100;

          for (let i = startTime; i < endTime; i += 100) {
            timeArr.push(i);
            timeArr.push(i + 30);
          }
        } else {
          startTime = null;
          endTime = null;
        }
        break;
      case "friday":
        if (coach.availability.friday.startTime) {
          startTime =
            Number(coach.availability.friday.startTime.slice(0, 2)) * 100;
          endTime = Number(coach.availability.friday.endTime.slice(0, 2)) * 100;

          for (let i = startTime; i < endTime; i += 100) {
            timeArr.push(i);
            timeArr.push(i + 30);
          }
        } else {
          startTime = null;
          endTime = null;
        }
        break;
      case "saturday":
        if (coach.availability.saturday.startTime) {
          startTime =
            Number(coach.availability.saturday.startTime.slice(0, 2)) * 100;
          endTime =
            Number(coach.availability.saturday.endTime.slice(0, 2)) * 100;

          for (let i = startTime; i < endTime; i += 100) {
            timeArr.push(i);
            timeArr.push(i + 30);
          }
        } else {
          startTime = null;
          endTime = null;
        }
        break;
      case "sunday":
        if (coach.availability.sunday.startTime) {
          startTime =
            Number(coach.availability.sunday.startTime.slice(0, 2)) * 100;
          endTime = Number(coach.availability.sunday.endTime.slice(0, 2)) * 100;

          for (let i = startTime; i < endTime; i += 100) {
            timeArr.push(i);
            timeArr.push(i + 30);
          }
        } else {
          startTime = null;
          endTime = null;
        }
        break;
      default:
        return "no data";
    }
    setTimes((times) => times.concat(timeArr).sort((a, b) => a - b));
    return timeArr;
  };

  const numberToTimeString = (number) => {
    let timeString = "";
    let hour = number / 10;
    hour = hour.toString().split("");
    if (hour.length < 3) {
      hour.unshift("0");
    }
    hour.push("0");
    hour.splice(2, 0, ":");
    timeString = hour.join("");

    return timeString;
  };

  return (
    <div id="container">
      <h3>Book a meeting by date or instructor</h3>
      <br />
      <Form id="changeInfo" onSubmit={handleSubmit}>
        <Label>Date: </Label>
        <Input
          type="date"
          placeholder="Search By Date"
          value={date}
          onChange={(e) => {
            e.preventDefault();
            localStorage.setItem("currentDate", e.target.value);
            localStorage.setItem("dayOfWeek", dateStringToDay(e.target.value));
            setDate(e.target.value);
          }}
        ></Input>
        <br />
        <br />
        <Label>Choose instructor: </Label>
        <select
          name="instructors"
          id="instructors"
          value={instructor}
          onChange={(e) => {
            e.preventDefault();
            setInstructor(e.target.value);
            localStorage.setItem("chosenCoach", e.target.value);
          }}
        >
          {!coaches
            ? null
            : coaches.map((coach) => {
                return (
                  <option value={coach.username} key={v4()}>
                    {coach.name}
                  </option>
                );
              })}
        </select>
        <br />
        <Label>Available times: </Label>
        <select
          name="meetingtime"
          id="meetingtime"
          onChange={(e) => {
            e.preventDefault();
            setMeetingTime(e.target.value);
            localStorage.setItem("chosenTime", e.target.value);
          }}
        >
          {times.map((time) => {
            let newTime = numberToTimeString(time);
            return (
              <option value={newTime} key={v4()}>
                {newTime}
              </option>
            );
          })}
        </select>
        <br />
        <br />
        <a href="/scheduleMeeting">
          Schedule a Meeting
        </a>
      </Form>
    </div>
  );
};

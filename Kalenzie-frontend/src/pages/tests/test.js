import React, {useState} from 'react';
import store from '../../store';
import {v4} from 'uuid';

export const TestPage = () => {
const [times, setTimes] = useState([]);
const [coaches, setCoaches] = useState(null)
const [day, setDay] = useState('monday')

const getDayList = async(day) => {
  await store.getCoaches();
  setCoaches(localStorage.coaches)
}

const getDailyTimes = (coach, day) => {
  let timeArr = [];
  let startTime = null;
  let endTime = null;
  switch(day) {
    case 'monday':
      if(coach.availability.monday.startTime){
        startTime = Number(coach.availability.monday.startTime.slice(0,2)) * 100;
        endTime = Number(coach.availability.monday.endTime.slice(0,2)) * 100;
        console.log(`${coach.name} is available from ${startTime} to ${endTime}`);
            for(let i = startTime; i < endTime; i+=100){
      timeArr.push(i)
      timeArr.push(i + 30)
    }
      }else{
        startTime = null;
        endTime = null
      }
      break
    case 'tuesday':
      if(coach.availability.tuesday.startTime){
        startTime = Number(coach.availability.tuesday.startTime.slice(0,2)) * 100;
        endTime = Number(coach.availability.tuesday.endTime.slice(0,2)) * 100;
        console.log(`${coach.name} is available from ${startTime} to ${endTime}`);
            for(let i = startTime; i < endTime; i+=100){
      timeArr.push(i)
      timeArr.push(i + 30)
    }
      }else{
        startTime = null;
        endTime = null
      }
      break
    case 'wednesday':
      if(coach.availability.wednesday.startTime){
        startTime = Number(coach.availability.wednesday.startTime.slice(0,2)) * 100;
        endTime = Number(coach.availability.wednesday.endTime.slice(0,2)) * 100;
        console.log(`${coach.name} is available from ${startTime} to ${endTime}`);
            for(let i = startTime; i < endTime; i+=100){
      timeArr.push(i)
      timeArr.push(i + 30)
    }
      }else{
        startTime = null;
        endTime = null
      }
      break
    case 'thursday':
      if(coach.availability.thursday.startTime){
        startTime = Number(coach.availability.thursday.startTime.slice(0,2)) * 100;
        endTime = Number(coach.availability.thursday.endTime.slice(0,2)) * 100;
        console.log(`${coach.name} is available from ${startTime} to ${endTime}`);
            for(let i = startTime; i < endTime; i+=100){
      timeArr.push(i)
      timeArr.push(i + 30)
    }
      }else{
        startTime = null;
        endTime = null
      }
      break
    case 'friday':
      if(coach.availability.friday.startTime){
        startTime = Number(coach.availability.friday.startTime.slice(0,2)) * 100;
        endTime = Number(coach.availability.friday.endTime.slice(0,2)) * 100;
        console.log(`${coach.name} is available from ${startTime} to ${endTime}`);
            for(let i = startTime; i < endTime; i+=100){
      timeArr.push(i)
      timeArr.push(i + 30)
    }
      }else{
        startTime = null;
        endTime = null
      }
      break
    case 'saturday':
      if(coach.availability.saturday.startTime){
        startTime = Number(coach.availability.saturday.startTime.slice(0,2)) * 100;
        endTime = Number(coach.availability.saturday.endTime.slice(0,2)) * 100;
        console.log(`${coach.name} is available from ${startTime} to ${endTime}`);
            for(let i = startTime; i < endTime; i+=100){
      timeArr.push(i)
      timeArr.push(i + 30)
    }
      }else{
        startTime = null;
        endTime = null
      }
      break
    case 'sunday':
      if(coach.availability.sunday.startTime){
        startTime = Number(coach.availability.sunday.startTime.slice(0,2)) * 100;
        endTime = Number(coach.availability.sunday.endTime.slice(0,2)) * 100;
        console.log(`${coach.name} is available from ${startTime} to ${endTime}`);
            for(let i = startTime; i < endTime; i+=100){
      timeArr.push(i)
      timeArr.push(i + 30)
    }
      }else{
        startTime = null;
        endTime = null
      }
      break
    default:
      console.log(`no data`)
    }
    console.log(coach.name, day, timeArr)
    setTimes(times => times.concat(timeArr).sort((a,b)=> a-b))
    // console.log(`Times: ${times}`)
    return timeArr
}

const seeCoaches = (day) => {
  console.log(JSON.parse(coaches))
  let parsedCoaches = JSON.parse(coaches)
  parsedCoaches.map(coach => {
    getDailyTimes(coach, day)
  })
  return parsedCoaches
}

const makeSelectBox = (arr) => {
  let newTimeArr = []
  arr.sort((a,b) => a - b).map((item) => {
    if(newTimeArr.indexOf(item) < 0){
    newTimeArr.push(item)}
  })
  console.log(newTimeArr)
  return newTimeArr
}

  return(
    <>
    <h1>Test</h1>
    <select value={day} onChange={e=> setDay(e.target.value)}>
      <option value="monday">Mon</option>
      <option value="tueday">Tues</option>
      <option value="wednesday">Wed</option>
      <option value="thursday">Thu</option>
      <option value="friday">Fri</option>
      <option value="saturday">Sat</option>
      <option value="sunday">Sun</option>
    </select>
    <button onClick={e=>{
      e.preventDefault()
      getDayList(day)
    }}>Get {day}</button>
    <button onClick={e=> {
      e.preventDefault()
      seeCoaches(day)}}>Console Log Coaches</button>
      <button onClick={() => console.log(times)}>Check Times Array</button>
    <h3>Times for {day}:</h3>
    <select>
    {makeSelectBox(times).map((item) => <option value={item} key={v4()}>{item}</option>)}
    </select>
    </>
  )
}
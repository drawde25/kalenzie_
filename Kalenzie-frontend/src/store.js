import { makeAutoObservable, toJS, configure } from 'mobx';
const apiURL = 'http://localhost:5000/';

configure({ enforceActions: false })
class Store {
    currentUser = null;
    counter = 0;
    users = null;
    date = ''
    constructor() {
        makeAutoObservable(this);
    }

    increment = () => {
        this.counter++;
    }
    setDate = (info) => {
        this.date = info.dateStr
    }
    getUsers = () => {
        // console.log(this.users);
        const options = {
            method: "GET",
        }
        const request = new Request(apiURL + 'findUsers', options);
        fetch(request)
            .then(res => res.json())
            .then(res => {
                this.users = res;
                console.log(toJS(this.users))
            })
            .catch(error => { console.error(error) })
        return
    }

    updateUserAvailability = (username, body) => {
        console.log(JSON.stringify(body))
        const options = {
            method: "PATCH",
            body: JSON.stringify(body),
            headers: {
                'Content-Type': 'application/json'
            },
        }
        console.log(`Body from store.js: ${JSON.stringify(body)}`)
        const request = new Request(apiURL + 'users/' + username, options);
        console.log(`request: ${request}`)
        fetch(request)
            .then(res => res.json())
            .then(res => {
                this.currentUser = res;
                console.log(toJS(this.currentUser))
            })
            // fetch(`${apiURL}users/${username}`, options)
            .catch((err) => { console.log(err) })
    }

    updatePhoto = (username, body) => {
        console.log(JSON.stringify(body))
        const options = {
            method: "PATCH:",
            body: JSON.stringify(body),
            headers: {
                'Content-Type': 'multipart/form-data'
            },
        }
        const request = new Request(apiURL + 'users/' + username, options);
        fetch(request)
            .then(res => res.json())
            .then(res => {
                this.currentUser = res;
                console.log(toJS(this.currentUser))
            })
            .catch((err) => { console.log(err) })
    }

    addUser = (body) => {
        const options = {
            method: "POST",
            body
        }
        const request = new Request(apiURL + 'addUser', options);
        fetch(request)
            .then(console.log(body))
    }

    getUser = (username) => {
        const options = {
            method: "GET",
            username
        }
        const request = new Request(apiURL + 'users/' + username, options);
        fetch(request)
            .then(res => res.json())
            .then(data => this.currentUser = data)
            // .then(() => console.log(toJS(this.currentUser)))
    }

    getCoaches = () => {
        const options = {
            method: "GET"
        }
        const request = new Request(apiURL + 'getCoaches', options)
        fetch(request)
            .then(res => res.json())
            .then(res => {
                localStorage.setItem('coaches', JSON.stringify(res))
                    // console.log(JSON.stringify(res))
                return res
            })
            .catch(err => console.log(err))
    }

}

const store = new Store();

export default store;

window.store = store;
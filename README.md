# Kalenzie

## description 
What problem are you solving?

Kenzie Academy is stacked with great instructors, facilitators, and tudors! With the growing number of students, their time is even more valuable. It is very nice to be able to set up meetings easily with an individual member of the faculty using Calendly, but wouldn’t it be even more convenient if there was a central calendar showing all currently available times with any instructor? Kalenzie is that calendar. 

What is your solution?

Kalenzie offers one central platform for our students to see all currently available meeting times, between all eligible students and teachers. You don’t have to search for each instructor individually to check for available meeting times. Kalenzie allows you to pick a time and matches you with an available coach. This makes scheduling and time-management easier on the coach, as well as the student.

## Technologies
JavaScript, HTML/CSS, React, Node.js
## Illustrations
![Screenshot__53_](/uploads/84a7f324ee81fbb10c1c79497659197b/Screenshot__53_.png)

![Screenshot__54_](/uploads/70b3e9af601e21fa57d18d5cac1d76a0/Screenshot__54_.png)

![Screenshot__55_](/uploads/08d570c17ce386480964822b7ccb135e/Screenshot__55_.png)

![Screenshot__56_](/uploads/8d7f411d0e4c8247014022e51150959b/Screenshot__56_.png)

![Screenshot__57_](/uploads/0aead575b5821eed20219341b3eb5e50/Screenshot__57_.png)

![Screenshot__58_](/uploads/aaf41fc4039349e999c2a7cd6e71b78b/Screenshot__58_.png)

![Screenshot__59_](/uploads/8bb222ef3220138d2c13b9c7cadc91ea/Screenshot__59_.png)

![Screenshot__60_](/uploads/d66b99f536cadae8f12258e2a1fcb032/Screenshot__60_.png)

![Screenshot__62_](/uploads/d8bc48afabcb001c53a5ba8e48e8ed8f/Screenshot__62_.png)

![Screenshot__64_](/uploads/d569ebacc78061859b6f978c9c0742fb/Screenshot__64_.png)

![Screenshot__69_](/uploads/a4b66be7855319a0743b3e0f77cef8ea/Screenshot__69_.png)

![Screenshot__72_](/uploads/681300b4fa1dc6c249db00fde64d4986/Screenshot__72_.png)

![Screenshot__75_](/uploads/d5c05864c05c1612a4af28d0490db537/Screenshot__75_.png)

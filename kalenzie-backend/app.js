const mongoose = require('mongoose');
const createError = require('http-errors');
const express = require('express');
const app = express();
const cors = require('cors');
const request = require('request');
const port = 5000;
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const url = 'mongodb+srv://drawde25:ghostkhat@cluster0.vs3kd.mongodb.net/Kapstone?retryWrites=true&w=majority'
mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true });
const db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error'));
db.once('open', function() {
    console.log(`Connection successful to mongoose`);
})
const UserSchema = new mongoose.Schema({
    name: {
        type: String,
        trim: true,
        required: true
    },
    username: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    email: {
        type: String,
        unique: true,
        required: true
    },
    role: {
        type: String,
        default: 'student',
        enum: ['student', 'coach'],
        required: true
    },
    bio: {
        type: String
    },
    quarter: {
        type: Number,
        default: 1,
        enum: [1, 2, 3, 4],
    },
    photo: {
        type: String
    },
    availability: {
        monday: {
            startTime: { type: String, default: null },
            endTime: { type: String, default: null }
        },
        tuesday: {
            startTime: { type: String, default: null },
            endTime: { type: String, default: null }
        },
        wednesday: {
            startTime: { type: String, default: null },
            endTime: { type: String, default: null }
        },
        thursday: {
            startTime: { type: String, default: null },
            endTime: { type: String, default: null }
        },
        friday: {
            startTime: { type: String, default: null },
            endTime: { type: String, default: null }
        },
        saturday: {
            startTime: { type: String, default: null },
            endTime: { type: String, default: null }
        },
        sunday: {
            startTime: { type: String, default: null },
            endTime: { type: String, default: null }
        },
    }
})

const User = mongoose.model("User", UserSchema)



const MeetingSchema = new mongoose.Schema({
    time: {
        type: String,
        required: true,
    },
    date: {
        type: String,
        required: true,
    },
    username: {
        type: String,
        required: true,
    },
    message: {
        type: String,
        required: true,
    },
    meetingWith: {
        type: String,
        required: true,
    },
    status: {
        type: String,
        required: true
    }
})



const Meeting = mongoose.model("Meeting", MeetingSchema)
const NotifactionSchema = new mongoose.Schema({
    time: {
        type: String,
        required: true,
    },
    date: {
        type: String,
        required: true,
    },
    username: {
        type: String,
        required: true,
    },
    message: {
        type: String,
        required: true,
    },
    meetingWith: {
        type: String,
        required: true,
    },
    meetingId: {
        type: String,
        required: true,
    }
})

const Notifaction = mongoose.model("Notifaction", NotifactionSchema)
app.use(function(req, res, next) {
        res.header('Access-Control-Allow-Origin', '*')
        res.header('Access-Control-Allow-Headers', 'Origin, X-Request-With, Content-Type, Accept')
        next()
    })
    //https://www.youtube.com/watch?v=gPzMRoPDrFk time stamp 2:41^^^^
app.use(cors())

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());

// {
//     "time": "2/2/2020",
//     "date": "yes",
//     "username": "yes",
//     "message": "yes",
//     "meetingWith": "yes",
//     "status": "confirmed"
//   }
app.get('/', (req, res, ) => {
    res.send('Welcome to Kalenzie again')
})
app.post('/addNotifaction', (req, res) => {
    const notifaction = new Notifaction(req.body)
    notifaction.save(function(err) {
        if (err) return console.log(err)
    })
    console.log(notifaction)
    res.status(201).send(notifaction)
})
app.get('/getNotifaction/:username', async(req, res) => {
    try {
        const notifactions = await Notifaction.find({ username: req.params.username })
        console.log(notifactions)
        res.send(notifactions)
    } catch (err) {
        return console.log(err)
    }
})
app.delete('/deleteNotifaction/:id', (req, res) => {
    Notifaction.findByIdAndRemove({ _id: req.params.id }).then(function(item) {
        res.send(item)
    })
})

app.patch('/users/:username', async(req, res) => {
    console.log(`from app.js patch request: ${req.body}`)
    try {
        const info = await User.findOneAndUpdate({ username: req.params.username }, req.body, { new: true })
            // console.log(`Info: ${info}`)
        res.status(201)
        res.send(info)
    } catch (err) {
        console.log(err)
    }
})

app.post('/addMeeting', (req, res) => {
    const meeting = new Meeting(req.body)
    meeting.save(function(err, meeting) {
        if (err) return console.log(err)
    })
    console.log(meeting)
    res.status(201).send(meeting)
})

app.patch('/updateMeeting/:id', async(req, res) => {
    try {
        const info = await Meeting.findOneAndUpdate({ _id: req.params.id }, req.body)
        res.status(201)
        res.send(info)
    } catch (err) {
        console.log(err)
    }

})


app.get('/getMeeting/:username', async(req, res) => {
    try {
        const meetings = await Meeting.find({ username: req.params.username })
        console.log(meetings)
        res.send(meetings)
    } catch (err) {
        console.log(err)
    }
})
app.get('/getMeetingI/:id', async(req, res) => {
    try {
        const meetings = await Meeting.find({ _id: req.params.id })
        console.log(meetings)
        res.send(meetings)
    } catch (err) {
        console.log(err)
    }
})
app.delete('/deleteMeeting/:id', (req, res, next) => {
    Meeting.findByIdAndRemove({ _id: req.params.id }).then(function(item) {
        res.send(item)
    })
})
app.post('/newUser', (req, res) => {
    const user = new User(req.body)
    user.save(function(err, users) {
        if (err) return console.error(err)
    })
    console.log(user)
    res.status(201).send(user)
})

app.get('/findUsers', async(req, res) => {
    let users = await User.find((err, data) => {
        if (err) return console.log(err)
        console.log(`Users: ${data}`)
    })
    console.log(users)
    res.send(users)
})
app.get('/findUsers/:email', async(req, res) => {
    try {
        const user = await User.find({ email: req.params.email })
        res.status(201)
        res.send(user)

    } catch (err) {
        console.log(err)
    }
})
app.get('/findUsersU/:username', async(req, res) => {
    try {
        const user = await User.find({ username: req.params.username })
        res.status(201)
        res.send(user)

    } catch (err) {
        console.log(err)
    }
})

app.get('/users/:username', async(req, res) => {
    let user = await User.findOne({ username: req.params.username }, (err, doc) => {
        if (err) return console.log(err)
        console.log(doc)
        res.send(doc)
    })
})

app.get('/getCoaches', async(req, res) => {
    try {
        let coaches = await User.find({ role: "coach" })
            // localStorage.setItem('coaches', JSON.stringify(coaches))
        res.send(coaches)
    } catch (err) {
        res.send(err)
    }
})
app.patch('/updateUser/:username', async(req, res) => {
    try {
        const info = await User.findOneAndUpdate({ username: req.params.username }, req.body)
        res.status(201)
        res.send(info)

    } catch (err) {
        console.log(err)
    }
})


app.use(function(req, res, next) {
    next(createError(404));
});

app.use(function(err, req, res, next) {
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    res.status(err.status || 500);
    res.send('error')
})

module.exports = app;

app.listen(port, () => console.log(`App listening on port: ${port}`))
import express from 'express';
import { UserModel } from '../schemas'

export default function userController(app) {
    const router = express.Router();

    router.get('/', async(req, res) => {
        try {
            const users = await UserModel.find();
            res.send(users);
        } catch (err) {
            console.error(err);
            res.status(500).send('Internal Server Error');
        }
    });

    router.post('/newUser', async(req, res) => {
        try {
            const user = await UserModel.create(req.body)
            user.save(function(err, users) {
                if (err) return console.error(err)
            })
            console.log(user)
            res.status(201).send(user)
        } catch (err) {
            console.error(err);
            res.status(500).send('Internal Server Error')
        }
    });

    app.use('/users', router)
}
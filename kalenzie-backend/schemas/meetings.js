const mongoose = require('mongoose')

const MeetingSchema = new mongoose.Schema({
    time: {
        type: String,
        required: true,
    },
    date: {
        type: String,
        required: true,
    },
    username: {
        type: String,
        required: true,
    },
    message: {
        type: String,
        required: true,
    },
    meetingWith: {
        type: String,
        required: true,
    },
    status: {
        type: String,
        required: true
    }
})

export const MeetingModel = mongoose.model("Meeting", MeetingSchema)
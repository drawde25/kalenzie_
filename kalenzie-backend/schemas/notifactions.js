const mongoose = require('mongoose')

const NotifactionSchema = new mongoose.Schema({
    time:{
        type: String,
        required: true, 
     },
     date: {
         type: String,
         required: true,
     },
     username:{
         type: String,
         required: true,
     },
     message:
     {
         type: String,
         required: true,
     },
     meetingWith:
     {
         type:String,
         required: true,
     }
})

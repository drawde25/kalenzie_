const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    name: {
        type: String,
        trim: true,
        required: true
    },
    username: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    email: {
        type: String,
        unique: true,
        required: true
    },
    role: {
        type: String,
        default: 'student',
        enum: ['student', 'coach'],
        required: true
    },
    bio: {
        type: String
    },
    quarter: {
        type: Number,
        default: 1,
        enum: [1, 2, 3, 4],
    },
    photo: {
        type: String
    }
})

const UserModel = mongoose.model("User", UserSchema)